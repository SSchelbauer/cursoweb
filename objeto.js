const produto = new Object
produto.preco = 220
produto.nome = 'Cadeira'
produto['marca do produto'] = 'Generica'

console.log(produto)
delete produto.preco
delete produto['marca do produto']
console.log(produto)

const carro = {
    modelo : 'A4',
    valor : 89000,
    proprietario: {
        nome : 'Raul',
        idade : 56,
        endereco: {
            logradouro: 'Rua',
            numero: 123
        }
    },
    condutores: [{
        nome: 'junior',
        idade: 19
    },{nome : 'ana', idade: 42}],
    
    calcularValorSeguro: function(){

    }
}

carro.proprietario.endereco.numero = 1000

console.log(carro)

