//sem promise..
const http = require('http')
const getTurma = (letra, callback) => {
    const url = `http://localhost:9876/turma${letra}.json`
    //const url = `C:\Temp\turmaA.json`
    
    
    http.get(url, res => {
        let resultado = ''

        res.on('data', dados => {
            resultado += dados
        })

        res.on('end', () => {
            callback(JSON.parse(resultado))
        })

        res.on('error', erro => {
            console.log(erro)
        })
    })
}

let nomes = []
getTurma('A', alunos => {
    console.log(alunos)
})