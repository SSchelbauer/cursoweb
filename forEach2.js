Array.prototype.forEach2 = function(callback) {
    for (let i = 0; i < this.length; i++) {
        callback(this[i], i, this)
    }
}

const aprovados = ['Agatha','Aldo', 'Daniel','Raquel']

aprovados.forEach2(function(n, i){
    console.log(`${i+1}) ${n}`)
})


for (let i = 0; i < aprovados.length; i++) {
    console.log(i, aprovados[i])
}




